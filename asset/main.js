var myArray = [];

//
function addNumber() {
    numberElement = document.querySelector("#number");

    if (numberElement.value == "") {
        return;
    }
    var number = numberElement.value * 1;
    myArray.push(number);
    numberElement.value = "";
    var contentElement = document.querySelector("#content");

    contentElement.innerHTML = `Phần tử được thêm vào mảng là : ${myArray}`;
}
// bài 1
function sumInteger() {
    sum = 0;

    for (var i = 0; i < myArray.length; i++) {
        var number = myArray[i];
        if (number > 0) {
            sum += number;
        }
    }
    document.querySelector("#content1").innerHTML = `Tổng số dương : ${sum}`;
}
//bài 2
function countInteger() {
    count = 0;

    for (var i = 0; i < myArray.length; i++) {
        var number = myArray[i];
        if (number > 0) {
            count++;
        }
    }

    document.querySelector(
        "#content2"
    ).innerHTML = `Đếm số dương có trong mảng : ${count}`;
}
//bài 3
function minNumber() {
    var negative = myArray[0];

    for (var i = 0; i < myArray.length; i++) {
        var number = myArray[i];

        if (number < negative) {
            negative = number;
        }
    }
    document.querySelector(
        "#content3"
    ).innerHTML = `Số nhỏ nhất trong mảng là : ${negative} `;
}
// bài 4
function minInteger() {
    var integer = myArray[0];

    for (var i = 0; i < myArray.length; i++) {
        var number = myArray[i];
        if (number < integer && number > 0) {
            integer = number;
        }
    }
    document.querySelector(
        "#content4"
    ).innerHTML = `Số dương nhỏ nhất trong mảng là : ${integer} `;
}
// bài 5
function evenNumber() {
    var lastNumber = myArray[myArray.length - 1];
    var evenNumber = 0;
    for (var i = 0; i < myArray.length; i++) {
        if (lastNumber % 2 === 0) {
            evenNumber = lastNumber;
        } else {
            evenNumber = "";
        }
    }
    document.querySelector(
        "#content5"
    ).innerHTML = `Số chẵn cuối cùng là : ${evenNumber} `;
}
//bài 6
function swapNumber() {
    var number1Element = document.getElementById("number-1").value * 1;
    var number2Element = document.getElementById("number-2").value * 1;

    var value1 = myArray[number1Element];
    myArray[number1Element] = myArray[number2Element];
    myArray[number2Element] = value1;

    document.querySelector(
        "#content6"
    ).innerHTML = `Vị trí sau khi đổi chỗ là :  ${myArray}`;
}

//bài 7
function sortNumber() {
    myArray.sort(function(a, b) {
        return a - b;
    });
    document.querySelector("#content7").innerHTML = `${myArray}`;
}
// bài 8
function searchNumber() {
    var result = 0;

    for (var i = 0; i < myArray.length; i++) {
        var currentValue = myArray[i];

        var check = checkSoNguyenTo(currentValue);

        if (check) {
            result = currentValue;
            break;
        } else {
            result = -1;
        }
    }

    document.querySelector("#content8").innerHTML = `${result}`;
}

function checkSoNguyenTo(num) {
    // return check;
    // if (num === 2) {
    //     return true;
    // } else if (num > 1) {
    //     for (var i = 2; i < num; i++) {
    //         if (num % i !== 0) {
    //             return true;
    //         } else if (num === i * i) {
    //             return false;
    //         } else {
    //             return false;
    //         }
    //     }
    // } else {
    //     return false;
    // }
    for (var i = 2, s = Math.sqrt(num); i <= s; i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return num > 1;
}
// Bài 9
var arr = [];

function countShow() {
    var number3Element = document.querySelector("#number-3");

    if (number3Element.value == "") {
        return;
    }
    var number = number3Element.value * 1;
    arr.push(number);

    number3Element.value = "";
    var countNumber = 0;
    for (var i = 0; i < arr.length; i++) {
        var number = arr[i];
        if (Number.isInteger(number)) {
            countNumber++;
        }
    }
    var result = document.querySelector("#content9");
    result.innerHTML = `<p>Mảng khi thêm để đếm : ${arr} </p>
    <p> Tổng số nguyên đếm được là : ${countNumber} </p>
    `;
}
// bài 10
function compare() {
    var countInteger = 0;
    var countNegative = 0;
    for (var i = 0; i < myArray.length; i++) {
        var number = myArray[i];

        if (number > 0) {
            countInteger++;
        } else {
            countNegative++;
        }
        if (countInteger > countNegative) {
            document.querySelector("#content10").innerHTML = `Số dương > Số âm`;
        } else if (countInteger == countNegative) {
            document.querySelector("#content10").innerHTML = `Số dương = Số âm`;
        } else {
            document.querySelector("#content10").innerHTML = `Số dương < Số âm`;
        }
    }
}

// còn bài 6 , 8